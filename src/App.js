import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  let name = "One";
  const [total, setTotal] = useState(0);

  function onClickDecrease() {
    setTotal(total - 1);
  }

  function onClickIncrease() {
    setTotal(total + 1);
  }

  console.log("React render");

  return (
    <div className="App">
      <header className="App-header">
        <div>Hello {name}!</div>
        <div>Total: {total}</div>
        <div>
          <button onClick={onClickDecrease}>-</button>{" "}
          <button onClick={onClickIncrease}>+</button>
        </div>
      </header>
    </div>
  );
}

export default App;
